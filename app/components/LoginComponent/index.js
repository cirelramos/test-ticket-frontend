/**
 *
 * LoginComponent
 *
 */

import React, { memo, useState } from 'react';
import { Button, Checkbox, Col, Form, Input, Row, Select } from 'antd';
import Title from 'antd/lib/typography/Title';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

const { Option } = Select;

const layout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 12,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 0,
    span: 24,
  },
};
const styleColSpanLogin = {
  textAlign: 'center',
  marginTop: '50px',
};
const styleBackgroundLogin = {
  background: '#fefefe',
  height: '100vh',
};

const LoginComponent = props => {
  const [language, setDataLanguage] = useState(props.selectedLanguage);
  const [email, setDataEmail] = useState('');
  const [password, setDataPassword] = useState('');
  const [remember, setDataRemember] = useState(false);
  const [disabled, setDataDisabled] = useState(false);

  const validate = () => {
    if (email.length > 0 && password.length > 0 && language != null) {
      setDataDisabled(false);
    } else {
      setDataDisabled(true);
    }
  };

  const handleSubmit = () => {
    const data = {
      email,
      password,
      remember,
      language,
    };
    const payload = { data, language };
    validate();
    if (disabled === false) props.login(payload);
  };

  const onFinish = values => {
    // handleSubmit();
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Row style={styleBackgroundLogin}>
      <Col span={8} />
      <Col span={8} style={styleColSpanLogin}>
        <img
          src="https://image.freepik.com/vector-gratis/plantilla-logotipo-envios_23-2147880407.jpg"
          alt=""
          width={160}
        />
        <Title level={2}>{props.intl.formatMessage({ id: 'app.login' })}</Title>
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: false,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label={props.intl.formatMessage({ id: 'app.email' })}
            name="email"
            value={email}
            onChange={e => {
              setDataEmail(e.target.value);
              validate();
            }}
            rules={[
              {
                required: true,
                message: props.intl.formatMessage({
                  id: 'app.please.input.your.email',
                }),
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label={props.intl.formatMessage({ id: 'app.password' })}
            name="password"
            value={password}
            // onPressEnter={handleSubmit}
            onChange={e => {
              setDataPassword(e.target.value);
              validate();
            }}
            rules={[
              {
                required: true,
                message: props.intl.formatMessage({
                  id: 'app.please.input.your.password',
                }),
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Select language"
              optionFilterProp="children"
              value={language}
              onChange={value => {
                setDataLanguage(value);
                validate();
                props.changeLocale(value);
              }}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              <Option value="en">
                {props.intl.formatMessage({ id: 'app.english' })}
              </Option>
              <Option value="es">
                {props.intl.formatMessage({ id: 'app.spanish' })}
              </Option>
            </Select>
          </Form.Item>
          <Form.Item {...tailLayout} name="remember" valuePropName="checked">
            <Checkbox>
              {props.intl.formatMessage({ id: 'app.remember.me' })}
            </Checkbox>
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              onClick={handleSubmit}
              // disabled={disabled}
            >
              <FormattedMessage id="app.login" />
            </Button>
          </Form.Item>
        </Form>
      </Col>
      <Col span={8} />
    </Row>
  );
};

LoginComponent.propTypes = {
  login: PropTypes.func.isRequired,
};

export default memo(LoginComponent);

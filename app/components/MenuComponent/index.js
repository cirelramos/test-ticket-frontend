/**
 *
 * MenuComponent
 *
 */

import React, { memo } from 'react';
import * as antd from 'antd';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

const { Layout, Menu, Breadcrumb } = antd;

const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;

const key = 'home';

const MenuComponent = props => (
  <Layout style={{ minHeight: '100vh' }}>
    <Header className="header" style={{ marginBottom: '55px' }}>
      <div className="logo" />
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
        <Menu.Item key="1">nav 1</Menu.Item>
        <Menu.Item key="2">nav 2</Menu.Item>
        <Menu.Item key="3" onClick={() => props.logoutResponse()}>
          logout
        </Menu.Item>
      </Menu>
    </Header>
    {props.component}
    <Footer style={{ textAlign: 'center' }}>
      Ant Design ©2018 Created by Ant UED
    </Footer>
  </Layout>
);

MenuComponent.propTypes = {};

export default memo(MenuComponent);

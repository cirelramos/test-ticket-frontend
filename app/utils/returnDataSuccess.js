export const validateDataResponse = dataResponse => {
  if (dataResponse != null) {
    if (typeof dataResponse === 'object') {
      if (dataResponse.code != null) {
        switch (dataResponse.code) {
          case 202:
          case 200:
            return dataResponse.data;
            break;
          default:
            return null;
            break;
        }
      }
    }
  }
  return null;
};

import collect from 'collect.js';
import * as antd from 'antd';

const { message } = antd;

export const success = content => {
  message.success(content, 2);
};

const error = content => {
  message.error(content, 2);
};

const warning = content => {
  message.warning(content, 2);
};

export const sendMessageResponse = dataResponse => {
  if (dataResponse != null) {
    if (typeof dataResponse === 'object') {
      if (dataResponse.code != null) {
        switch (dataResponse.code) {
          case 202:
          case 200:
            getSuccess(dataResponse);
            break;
          case 427:
          case 422:
          case 429:
            getWarnings(dataResponse);
            break;
          default:
            getErrors(dataResponse);
            break;
        }
      }
    }
  }
};

const getErrors = data => {
  if (data != null) {
    if (data.data != null) {
      showAlertWithArrayError(data);
      showAlertWithMesssageWithoutErrorElements(data);
    }
  }
};
const getWarnings = data => {
  if (data != null) {
    if (data.data != null) {
      showAlertWithArrayError(data, 'warning');
      showAlertWithMesssageWithoutErrorElements(data, 'warning');
      showAlertWithObjectError(data, 'warning');
    }
  }
};

const getSuccess = data => {
  if (data != null) {
    if (data.message != null) {
      success(data.message);
    }
  }
};
const showAlertWithMesssageWithoutErrorElements = (data, type = 'error') => {
  if (data.data.error == null && data.message != null && data.message !== '') {
    if (type === 'error') error(data.message);
    if (type === 'warning') warning(data.message);
  }
};
const showAlertWithArrayError = (data, type = 'error') => {
  if (data.data.error != null) {
    const collectError = collect(data.data.error);
    collectError.each(item => {
      if (type === 'error') error(item);
      if (type === 'warning') warning(item);
    });
  }
};

const showAlertWithObjectError = (data, type = 'error') => {
  if (data.data != null && data.data.error == null) {
    if (typeof data.data === 'object') {
      const collectError = collect(data.data);
      collectError.each(ErrorsValidate => {
        const collectValidate = collect(ErrorsValidate);
        collectValidate.each(item => {
          if (type === 'error') error(item);
          if (type === 'warning') warning(item);
        });
      });
    }
  }
};

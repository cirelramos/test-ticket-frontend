/*
 *
 * MenuContainer constants
 *
 */

export const DEFAULT_ACTION = 'app/MenuContainer/DEFAULT_ACTION';
export const WATCHER_LOGOUT_ACTION = 'app/MenuContainer/WATCHER_LOGOUT_ACTION';

import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the menuContainer state domain
 */

const selectMenuContainerDomain = state => state.menuContainer || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by MenuContainer
 */

const makeSelectMenuContainer = () =>
  createSelector(
    selectMenuContainerDomain,
    substate => substate,
  );

export default makeSelectMenuContainer;
export { selectMenuContainerDomain };

/**
 *
 * MenuContainer
 *
 */

import React, { memo, useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { Helmet } from 'react-helmet';
import makeSelectMenuContainer from './selectors';
import reducer from './reducer';
import saga from './saga';
import MenuComponent from '../../components/MenuComponent';
import { logoutWatcher } from '../LoginContainer/actions';
import { success } from '../../utils/messages';

export function MenuContainer(props) {
  /* injectt librarys */
  useInjectReducer({ key: 'menuContainer', reducer });
  useInjectSaga({ key: 'menuContainer', saga });

  /* hooks */
  const history = useHistory();
  const [collapse, setCollapse] = useState(false);

  const logoutResponse = () => {
    const token = localStorage.getItem('token');
    const data = [];
    const payload = { token, data };
    props.logoutWatcher(payload);
    success(props.intl.formatMessage({ id: 'app.session.ended' }));
    localStorage.removeItem('token');
    history.push('/login');
  };

  return (
    <div>
      <Helmet>
        <title>{props.intl.formatMessage({ id: 'app.menu' })}</title>
        <meta
          name="description"
          content={props.intl.formatMessage({ id: 'app.menu' })}
        />
      </Helmet>
      <MenuComponent logoutResponse={() => logoutResponse()} {...props} />;
    </div>
  );
}

MenuContainer.propTypes = {
  logoutWatcher: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  menuContainer: makeSelectMenuContainer(),
});

function mapDispatchToProps(dispatch) {
  return {
    logoutWatcher: params => dispatch(logoutWatcher(params)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  injectIntl,
  withConnect,
  memo,
)(MenuContainer);

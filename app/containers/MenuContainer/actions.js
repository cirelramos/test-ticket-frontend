/*
 *
 * MenuContainer actions
 *
 */

import { DEFAULT_ACTION, WATCHER_LOGOUT_ACTION } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function logoutWatcher(payload) {
  return {
    type: WATCHER_LOGOUT_ACTION,
    payload,
  };
}

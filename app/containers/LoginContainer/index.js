/**
 *
 * LoginContainer
 *
 */

import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Button } from 'antd';
import { makeSelectSuccessResponse } from './selectors';
import reducer from './reducer';
import saga from './saga';
import LoginComponent from '../../components/LoginComponent';
import { loginClear, loginWatcher } from './actions';
import { makeSelectLoadingResponse } from '../App/selectors';
import { validateDataResponse } from '../../utils/returnDataSuccess';
import { sendMessageResponse } from '../../utils/messages';
import { changeLocale } from '../LanguageProvider/actions';
import { makeSelectLocale } from '../LanguageProvider/selectors';

export function LoginContainer(props) {
  useInjectReducer({ key: 'loginContainer', reducer });
  useInjectSaga({ key: 'loginContainer', saga });
  const [dataResponse, setDataResponse] = useState(null);

  useEffect(() => {
    setDataResponse(validateDataResponse(props.successResponse));
    sendMessageResponse(props.successResponse);
    setDataToLocalStorageAndRedirectToHome(props.successResponse);
    props.loginClear([]);
  }, [props.successResponse]);

  const setDataToLocalStorageAndRedirectToHome = successReponse => {
    if (successReponse != null) {
      if (successReponse.code === 200) {
        const { access_token } = successReponse.data;
        localStorage.setItem('token', access_token);
        props.history.push('/');
      }
      if (localStorage.getItem('token') != null) {
        props.history.push('/');
      }
    }
  };
  return (
    <div>
      <Helmet>
        <title>{props.intl.formatMessage({ id: 'app.login' })}</title>
        <meta
          name="description"
          content={props.intl.formatMessage({ id: 'app.login' })}
        />
      </Helmet>

      <LoginComponent
        login={props.loginWatcher}
        changeLocale={props.changeLocale}
        selectedLanguage={props.selectedLanguage}
        {...props}
      />
    </div>
  );
}

LoginContainer.propTypes = {
  loginWatcher: PropTypes.func.isRequired,
  changeLocale: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  successResponse: makeSelectSuccessResponse(),
  loadingResponse: makeSelectLoadingResponse(),
  selectedLanguage: makeSelectLocale(),
});

function mapDispatchToProps(dispatch) {
  return {
    loginWatcher: params => dispatch(loginWatcher(params)),
    loginClear: params => dispatch(loginClear(params)),
    changeLocale: params => dispatch(changeLocale(params)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  injectIntl,
  withConnect,
  memo,
)(LoginContainer);

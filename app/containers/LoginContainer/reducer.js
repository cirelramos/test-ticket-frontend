/*
 *
 * LoginContainer reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOGIN_CLEAR,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  LOGIN_WATCHER,
  LOGOUT_WATCHER,
} from './constants';

export const initialState = fromJS({
  loading: false,
  login: false,
  error: false,
  dataRequest: [],
});

function loginContainerReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_WATCHER:
      return state.set('error', false).set('loading', true);
    case LOGIN_SUCCESS:
      return state
        .set('login', true)
        .set('dataRequest', action.payload)
        .set('loading', false);
    case LOGOUT_WATCHER:
      return state
        .set('login', false)
        .set('dataRequest', null)
        .set('loading', false);
    case LOGIN_FAILED:
      return state
        .set('login', false)
        .set('dataRequest', action.payload)
        .set('error', true)
        .set('loading', false);
    case LOGIN_CLEAR:
      return state
        .set('login', false)
        .set('dataRequest', null)
        .set('loading', false);
    default:
      return state;
  }
}

export default loginContainerReducer;

import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the loginContainer state domain
 */

const selectLoginContainerDomain = state =>
  state.loginContainer || initialState;

export const makeSelectSuccessResponse = () =>
  createSelector(
    selectLoginContainerDomain,
    loginState => loginState.get('dataRequest'),
  );

export const makeSelectLoginResponse = () =>
  createSelector(
    selectLoginContainerDomain,
    loginState => loginState.get('login'),
  );

/**
 * Other specific selectors
 */

/**
 * Default selector used by LoginContainer
 */

const makeSelectLoginContainer = () =>
  createSelector(
    selectLoginContainerDomain,
    substate => substate,
  );

export default makeSelectLoginContainer;
export { selectLoginContainerDomain };

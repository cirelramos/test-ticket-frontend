import { call, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import { loginFailed, loginSuccess } from './actions';
import { LOGIN_WATCHER, LOGOUT_WATCHER } from './constants';
import { constErrorResponseApi } from '../../utils/constError';

function loginRequest(payload) {
  return axios.request({
    method: 'POST',
    headers: { 'Content-Language': payload.language },
    url: `${process.env.URL_REST_API}oauth/login`,
    data: payload.data,
    validateStatus: status => true,
  });
}

function logoutRequest(payload) {
  return axios.request({
    method: 'GET',
    headers: { Authorization: `Bearer ${payload.token}` },
    url: `${process.env.URL_REST_API}oauth/logout`,
    data: payload.data,
    validateStatus: status => true,
  });
}

export function* loginSaga(action) {
  try {
    const result = yield call(loginRequest, action.payload);

    switch (result.status) {
      case 202:
        yield put(loginSuccess(result.data));
        break;
      default:
        yield put(loginFailed(result.data));
        break;
    }
  } catch (e) {
    yield put(loginFailed(constErrorResponseApi()));
  }
}

export function* logoutSaga(action) {
  try {
    const result = yield call(logoutRequest, action.payload);
  } catch (e) {
    console.log({ e });
  }
}

export default function* loginContainerSaga() {
  yield takeLatest(LOGIN_WATCHER, loginSaga);
  yield takeLatest(LOGOUT_WATCHER, logoutSaga);
}

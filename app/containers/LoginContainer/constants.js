/*
 *
 * LoginContainer constants
 *
 */

export const LOGIN_WATCHER = 'app/Login/LOGIN_WATCHER';
export const LOGOUT_WATCHER = 'app/Login/LOGOUT_WATCHER';
export const LOGIN_FAILED = 'app/Login/LOGIN_FAILED';
export const LOGIN_SUCCESS = 'app/Login/LOGIN_SUCCESS';
export const LOGIN_CLEAR = 'app/Login/LOGIN_CLEAR';

/*
 *
 * LoginContainer actions
 *
 */

import {
  LOGIN_CLEAR,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  LOGIN_WATCHER,
  LOGOUT_WATCHER,
} from './constants';

export function logoutWatcher(payload) {
  return {
    type: LOGOUT_WATCHER,
    payload,
  };
}

export function loginClear(payload) {
  return {
    type: LOGIN_CLEAR,
    payload,
  };
}
export function loginWatcher(payload) {
  return {
    type: LOGIN_WATCHER,
    payload,
  };
}

export function loginFailed(payload) {
  return {
    type: LOGIN_FAILED,
    payload,
  };
}

export function loginSuccess(payload) {
  return {
    type: LOGIN_SUCCESS,
    payload,
  };
}

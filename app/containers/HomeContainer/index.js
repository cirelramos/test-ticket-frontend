/**
 *
 * HomeContainer
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import {
  AppstoreOutlined,
  MailOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import * as antd from 'antd';
import { injectIntl } from 'react-intl';
import makeSelectHomeContainer from './selectors';
import reducer from './reducer';
import saga from './saga';

const { Layout, Menu } = antd;
const { Content, Sider } = Layout;

const { SubMenu } = Menu;

export const HomeContainer = props => {
  useInjectReducer({ key: 'homeContainer', reducer });
  useInjectSaga({ key: 'homeContainer', saga });
  console.log({ props });
  return (
    <div>
      <Helmet>
        <title>{props.intl.formatMessage({ id: 'app.home' })}</title>
        <meta
          name="description"
          content={props.intl.formatMessage({ id: 'app.home' })}
        />
      </Helmet>

      <Content style={{ padding: '0 50px' }}>
        <Layout
          className="site-layout-background"
          style={{ padding: '24px 0', background: 'white' }}
        >
          <Sider
            className="site-layout-background"
            width={200}
            style={{ background: 'white' }}
          >
            <Menu
              style={{ width: 200 }}
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              mode="inline"
            >
              <SubMenu
                key="sub2"
                title={
                  <span>
                    <AppstoreOutlined />
                    <span>Navigation Two</span>
                  </span>
                }
              >
                <Menu.Item key="5">Option 5</Menu.Item>
                <Menu.Item key="6">Option 6</Menu.Item>
                <SubMenu key="sub3" title="Submenu">
                  <Menu.Item key="7">Option 7</Menu.Item>
                  <Menu.Item key="8">Option 8</Menu.Item>
                </SubMenu>
              </SubMenu>
              <SubMenu
                key="sub4"
                title={
                  <span>
                    <SettingOutlined />
                    <span>Navigation Three</span>
                  </span>
                }
              >
                <Menu.Item key="9">Option 9</Menu.Item>
                <Menu.Item key="10">Option 10</Menu.Item>
                <Menu.Item key="11">Option 11</Menu.Item>
                <Menu.Item key="12">Option 12</Menu.Item>
              </SubMenu>
            </Menu>
          </Sider>

          <Content style={{ padding: '0 24px', minHeight: 280 }}>data</Content>
        </Layout>
      </Content>
    </div>
  );
};

HomeContainer.propTypes = {};

const mapStateToProps = createStructuredSelector({
  homeContainer: makeSelectHomeContainer(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  injectIntl,
)(HomeContainer);

import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { Layout } from 'antd';
import MenuContainer from '../MenuContainer';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      localStorage.getItem('token') ? (
        <Layout>
          <MenuContainer component={<Component {...props} />} {...props} />
        </Layout>
      ) : (
        <Redirect
          to={{ pathname: '/login', state: { from: props.location } }}
        />
      )
    }
  />
);

PrivateRoute.propTypes = {};

export default PrivateRoute;
